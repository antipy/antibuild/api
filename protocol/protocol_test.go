// Copyright © 2018 - 2019 Antipy V.O.F. info@antipy.com
//
// Licensed under the MIT License

package protocol

import (
	"io"
	"sync"
	"testing"
)

var (
	host   *Connection
	client *Connection
)

func TestProtocol(t *testing.T) {
	t.Run("Open", testOpen)
	t.Run("Init", testInit)
	t.Run("round", testRoundTrip)
}

func testOpen(t *testing.T) {
	hin, cout := io.Pipe()
	cin, hout := io.Pipe()

	host = OpenConnection(hin, hout)
	client = OpenConnection(cin, cout)
}

func testInit(t *testing.T) {
	wait := sync.WaitGroup{}
	wait.Add(1)
	go func() {
		i, err := host.Init(true, "1.0.0")
		if err != nil {
			t.Error(err)
		}
		if i.Protocol != version.Protocol {
			t.Error(i.Protocol)
		}
		if i.Module != version.Module {
			t.Error(i.Module)
		}

		wait.Done()
	}()

	i, err := client.Init(false, "1.0.0")
	if err != nil {
		t.Error(err)
	}
	if i.Protocol != version.Protocol {
		t.Error(i.Protocol)
	}
	if i.Module != version.Module {
		t.Error(i.Module)
	}

	wait.Wait()
}

func testRoundTrip(t *testing.T) {
	wait := sync.WaitGroup{}
	id := ID{2}
	function := "TestExecute"
	wait.Add(1)
	go func() {
		p := ExecuteMethod{Function: function}
		host.Send(ComExecute, p, id)
		resp, err := host.GetResponse()
		if err != nil {
			t.Error("error receiving data")
		}
		if resp.ID != id {
			t.Error("received wrong ID back")
		}
		var v bool
		err = host.com.GetData(&v)

		if err != nil {
			t.Error("received wrong data type back")
		}
		if !v {
			t.Error("received wrong value back")
		}
		wait.Done()
	}()
	message, err := client.Receive()
	if err != nil {
		t.Error("error receiving data")
	}
	if message.ID != id {
		t.Error("received wrong ID")
	}
	if message.Command != function {
		t.Error("received wrong command")
	}
	message.Respond(true, nil)
	wait.Wait()
}
