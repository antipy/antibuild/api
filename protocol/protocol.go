// Copyright © 2018 - 2019 Antipy V.O.F. info@antipy.com
//
// Licensed under the MIT License

package protocol

import (
	"bufio"
	"crypto/rand"
	"io"
	"sync"

	"github.com/jaicewizard/tt"
	"gitlab.com/antipy/antibuild/api/errors"
)

type (
	//Methods is a map of available commands to list of functions allowed to be called
	Methods map[string][]string

	//ReceiveMethods is the type used as payload for GetAll
	// the resposne should be of type Methods, where you can execute the method by sending key_methodname
	ReceiveMethods struct{}

	//Kill is the type used as payload for GetAll
	Kill struct{}

	//Version is the version type used for transmission of the version number
	Version struct {
		Protocol int
		Module   string
		HostPid  ID
	}

	//ExecuteMethod is the type for jsut executing a method
	ExecuteMethod struct {
		Function string
		Args     interface{}
	}

	//Payload is the payload converted to a token
	Payload interface {
		Execute() (Token, interface{})
	}

	//Message is a message to a client
	Message struct {
		Command string
		Token   Token
	}

	//Response is the response for a given command
	Response struct {
		ID  ID
		Log []errors.Error
	}

	//Token is a token used to receive data and send it back to the host
	Token struct {
		Command  string
		DataType string
		ID       ID
		con      *Connection
		Bin      []byte
	}
	//Connection is a connection
	Connection struct {
		com SendReceiver
		ID  string
	}

	dataIntf interface {
		getRaw(io.Reader) error
	}

	//ID is the type used for identification of the messages and in-procces cliend detection
	ID [10]byte

	//SendReceiver is an interface to send and receive messages and tokens etc
	SendReceiver interface {
		Send(Message, interface{}, ...byte) error
		Receive() (Token, error)
		Respond(Response, interface{}) error
		GetResponse() (Response, error)
		GetData(interface{}) error
	}

	sendReceiver struct {
		in     io.Reader
		inInit sync.Once
		reader *tt.V3Decoder

		out     io.Writer
		outInit sync.Once
		writer  *tt.V3Encoder

		rlock sync.Mutex
		wlock sync.Mutex
	}
)

const (
	//GetMethods is the command to get all the methods
	GetMethods = "getMethods"
	//KillCommand is the command to kill the connection
	KillCommand = "KILL"
	//ComVersion is the command to get the version(setup only)
	ComVersion = "getVersion"
	//ComExecute is the command to execute a function on the client
	ComExecute = "ExecuteMethod"
)

var (
	tokenGetVersion = Token{Command: ComVersion}
	tokenGetMethods = Token{Command: GetMethods}
	tokenKill       = Token{Command: KillCommand}

	//ErrEOF error for when reader closed
	ErrEOF = errors.New("could not get response", errors.CodeFatal)

	//version ID used for verifying versioning
	verifyVersionID = ID{1}
	version         = Version{
		Protocol: 1,
	}

	//ErrProtocolViolation is the error thrown whenever a protocol violation occurs
	ErrProtocolViolation = errors.New("the protocol is violated by the opposite party, either the version is incompatible or the module is not a module", errors.CodeProtocolFailure)

	//ErrFailedToSend is for when something failed to send
	ErrFailedToSend = errors.New("Failed to send the message", errors.CodeFatal)
)

//OpenConnection opens a new connection based on the inputs
func OpenConnection(in io.Reader, out io.Writer) *Connection {
	con := Connection{}
	com := &sendReceiver{}
	com.in = in
	com.out = out
	com.inInit = sync.Once{}
	com.outInit = sync.Once{}
	com.rlock = sync.Mutex{}
	com.wlock = sync.Mutex{}
	con.com = com
	return &con
}

var erroredVersion = Version{
	Protocol: 0,
	Module:   "error",
}

var setPID = sync.Once{}

//Init initiates the protocol with a version exchange, returns 0 when a protocol violation happens
func (c *Connection) Init(isHost bool, moduleVersion string) (Version, error) {
	setPID.Do(func() {
		_, _ = rand.Read(version.HostPid[:]) //should not be possible to throw error
	})

	version.Module = moduleVersion

	// Host version check
	if isHost {
		err := c.Send(ComVersion, version, verifyVersionID) // send client request for version message
		if err != nil {
			return erroredVersion, errors.New("failed while sending message to client", errors.CodeProtocolFailure)
		}

		resp, err := c.GetResponse() // wait for version response
		if err != nil {
			return erroredVersion, ErrProtocolViolation
		}

		if resp.ID != verifyVersionID {
			return erroredVersion, ErrProtocolViolation
		}

		v := Version{}
		err = c.com.GetData(&v)
		if err != nil {
			return erroredVersion, ErrProtocolViolation
		}

		// check if protocol version is correct
		if v.Protocol < version.Protocol {
			return v, errors.New("guest is using an older version of the protocol", errors.CodeProtocolFailure)
		}
		if v.Protocol > version.Protocol {
			return v, errors.New("guest is using a newer version of the protocol", errors.CodeProtocolFailure)
		}

		// check if module version is coirrect
		if v.Module != version.Module {
			return erroredVersion, errors.New("guest has the wrong module version", errors.CodeProtocolFailure)
		}

		return v, nil
	}

	message, err := c.Receive()
	if err != nil {
		return erroredVersion, ErrProtocolViolation
	}
	if message.ID != verifyVersionID {
		return erroredVersion, ErrProtocolViolation
	}
	if message.DataType != "version" {
		return erroredVersion, ErrProtocolViolation
	}
	v := Version{}
	err = c.com.GetData(&v)
	if err != nil {
		return erroredVersion, ErrProtocolViolation
	}

	if v.Protocol < version.Protocol {
		return erroredVersion, errors.New("host is using an older version of the protocol", errors.CodeProtocolFailure)
	}
	if v.Protocol > version.Protocol {
		return erroredVersion, errors.New("host is using a newer version of the protocol", errors.CodeProtocolFailure)
	}

	if v.Module != version.Module {
		return erroredVersion, errors.New("host is requesting a different module or a diferent version of this module", errors.CodeProtocolFailure)
	}

	err = message.Respond(version, nil)
	if err != nil {
		return erroredVersion, errors.New("failed to respond to client", errors.CodeProtocolFailure)
	}

	return v, nil
}

//HasSamePID will return true if the returned version has the same PID as the host
func (v Version) HasSamePID() bool {
	return v.HostPid == version.HostPid
}

//Receive waits for a command from the host to excecute
func (c *Connection) Receive() (Token, error) {
	token, err := c.com.Receive()

	token.con = c
	return token, err
}

//GetResponse waits for a response from the client
func (c *Connection) GetResponse() (Response, error) {
	return c.com.GetResponse()
}

//Send sends a command to the guest
func (c *Connection) Send(command string, payload Payload, id ID, binary ...byte) error {
	var message Message

	message.Command = command
	token, data := payload.Execute()
	token.ID = id
	token.Bin = binary
	message.Token = token

	err := c.com.Send(message, data, binary...)
	if err != nil {
		return err
	}
	return nil
}

//GetData agets the data corresponding with the token
func (c *Connection) GetData(e interface{}) error {
	return c.com.GetData(e)
}

//SetSendReceiver sets the send-receiver
func (c *Connection) SetSendReceiver(sr SendReceiver) {
	c.com = sr
}

func (sr *sendReceiver) Receive() (Token, error) {
	var command Message

	sr.inInit.Do(initIn(sr))

	sr.rlock.Lock()
	err := sr.reader.Decode(&command)
	sr.rlock.Unlock()

	token := command.Token
	return token, err
}

func (sr *sendReceiver) GetResponse() (Response, error) {
	var resp Response

	sr.inInit.Do(initIn(sr))

	sr.rlock.Lock()
	err := sr.reader.Decode(&resp)
	sr.rlock.Unlock()
	return resp, err
}

func (sr *sendReceiver) GetData(e interface{}) error {
	sr.inInit.Do(initIn(sr))

	sr.rlock.Lock()
	err := sr.reader.Decode(e)
	sr.rlock.Unlock()
	return err
}

func (sr *sendReceiver) Send(message Message, data interface{}, binary ...byte) error {
	sr.outInit.Do(initOut(sr))

	message.Token.Bin = binary

	sr.wlock.Lock()
	err := sr.writer.Encode(message)
	if err != nil {
		sr.wlock.Unlock()
		return err
	}
	if data != nil {
		err = sr.writer.Encode(data)
	}
	sr.wlock.Unlock()
	if err != nil {
		return err
	}

	return nil
}

func (sr *sendReceiver) Respond(resp Response, data interface{}) error {
	sr.outInit.Do(initOut(sr))

	sr.wlock.Lock()
	err := sr.writer.Encode(resp)
	if err != nil {
		sr.wlock.Unlock()
		return err
	}
	if data != nil {
		err = sr.writer.Encode(data)
	}
	sr.wlock.Unlock()
	if err != nil {
		return err
	}
	return nil
}

//GetData agets the data corresponding with the token
func (t *Token) GetData(e interface{}) error {
	return t.con.com.GetData(e)
}

//Error is a wrapper around Respond for ergonomics
func (t *Token) Error(log ...errors.Error) error {
	return t.Respond(nil, log)
}

//Succes is a wrapper around Respond for ergonomics
func (t *Token) Succes(data interface{}) error {
	return t.Respond(data, nil)
}

//Respond sends the given data back to the host
func (t *Token) Respond(data interface{}, log []errors.Error) error {

	if t.con == nil {
		return errors.New("connection is nil", errors.CodeFatal)
	}

	var resp Response
	resp.ID = t.ID
	resp.Log = log

	err := t.con.com.Respond(resp, data)
	if err != nil {
		return err
	}

	return nil
}

//Execute converts the Payload to a Token
func (gm ReceiveMethods) Execute() (Token, interface{}) {
	ret := tokenGetMethods
	return ret, nil
}

//Execute converts the Payload to a Token
func (gm Kill) Execute() (Token, interface{}) {
	ret := tokenKill
	return ret, nil
}

//Execute converts the Payload to a Token
func (v Version) Execute() (Token, interface{}) {
	ret := tokenGetVersion
	ret.DataType = "version"
	return ret, v
}

//Execute converts the Payload to a Token
func (gm ExecuteMethod) Execute() (Token, interface{}) {
	ret := Token{Command: gm.Function}

	return ret, gm.Args
}

func initOut(sr *sendReceiver) func() {
	return func() {
		sr.writer = tt.NewV3Encoder(sr.out, true)
	}
}
func initIn(sr *sendReceiver) func() {
	return func() {
		sr.reader = tt.NewV3Decoder(bufio.NewReader(sr.in), true)
	}
}
