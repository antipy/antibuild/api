module gitlab.com/antipy/antibuild/api

go 1.14

require (
	github.com/acomagu/bufpipe v1.0.3
	github.com/go-test/deep v1.0.6
	github.com/jaicewizard/tt v0.8.1
)
