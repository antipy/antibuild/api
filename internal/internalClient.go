// Copyright © 2018 - 2019 Antipy V.O.F. info@antipy.com
//
// Licensed under the MIT License

package internal

import (
	"errors"
	"reflect"
	"sync"

	"gitlab.com/antipy/antibuild/api/protocol"
)

/*
 this file is for modules that are internal to the process, this allows us to skip GOB and thus dramaticly
 reduce message send time and garbage creation
*/
type (
	//InternalConnection is a connection to an internal module
	InternalConnection struct {
		responses chan protocol.Response
		commands  chan protocol.Token
		data      chan interface{}
		respdata  chan interface{}
	}
)

var (
	lock sync.Mutex
	//loadedModules is an indication of what modudules are loaded in this process
	loadedModules = make(map[string]chan *InternalConnection)
)

//AddLoadedModules adds a loaded module
func AddLoadedModules(namever string) *InternalConnection {
	lock.Lock()
	if v, ok := loadedModules[namever]; ok {
		lock.Unlock()
		delete(loadedModules, namever)
		return <-v
	}
	con := &InternalConnection{
		responses: make(chan protocol.Response, 1),
		commands:  make(chan protocol.Token, 1),
		data:      make(chan interface{}, 1),
		respdata:  make(chan interface{}, 1),
	}
	loadedModules[namever] = make(chan *InternalConnection, 1)
	loadedModules[namever] <- con
	lock.Unlock()
	return &InternalConnection{
		responses: con.responses,
		commands:  con.commands,
		data:      con.respdata,
		respdata:  con.data,
	}
}

func (ic *InternalConnection) Receive() (protocol.Token, error) {
	return <-ic.commands, nil
}

func (ic *InternalConnection) GetResponse() (protocol.Response, error) {
	return <-ic.responses, nil
}

func (ic *InternalConnection) Send(message protocol.Message, data interface{}, binary ...byte) error {
	token := message.Token
	token.Bin = binary

	ic.commands <- message.Token
	if data != nil {
		ic.respdata <- data
	}
	return nil
}

func (ic *InternalConnection) Respond(resp protocol.Response, data interface{}) error {
	ic.responses <- resp
	if data != nil {
		ic.respdata <- data
	}
	return nil
}
func (ic *InternalConnection) GetData(e interface{}) error {
	data := <-ic.data
	if reflect.TypeOf(data) != reflect.TypeOf(e).Elem() && (reflect.TypeOf(e).Elem().Kind() != reflect.Interface || reflect.TypeOf(e).Elem().NumMethod() != 0) {
		return errors.New("Cannot load " + reflect.TypeOf(data).String() + " into " + reflect.TypeOf(e).Elem().String())
	}
	reflect.ValueOf(e).Elem().Set(reflect.ValueOf(data))
	e = &data
	return nil
}
