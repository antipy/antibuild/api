package api_test

import (
	"encoding/json"
	"fmt"
	"io"
	"testing"

	"github.com/jaicewizard/tt"

	"github.com/acomagu/bufpipe"
	abm "gitlab.com/antipy/antibuild/api/client"
	"gitlab.com/antipy/antibuild/api/host"
	"gitlab.com/antipy/antibuild/api/site"
)

func TestKill(t *testing.T) {
	hin, cout := io.Pipe()
	cin, hout := io.Pipe()

	complete := make(chan bool)
	go func() {
		module := abm.Register("TEST", "1.0.0")
		ret := make(chan int)
		go startwrap(module, cin, cout, ret)
		<-ret
		complete <- true
	}()

	module, err := host.Start(hin, hout, nil, "1.0.0", "")
	if err != nil {
		t.Fail()
	}
	module.Kill()

	if <-complete == false {
		t.Fail()
	}
}

func TestError(t *testing.T) {
	complete := make(chan bool)

	hin, cout := bufpipe.New([]byte{})
	cin, hout := bufpipe.New([]byte{})

	testTMP := func(w abm.TFRequest, r abm.Response) {
		r.AddFatal("test error")
	}

	go func() {
		module := abm.Register("TEST", "1.0.0")
		module.TemplateFunctionRegister("TESTFUNC", testTMP, &abm.TFTest{
			Request: abm.TFRequest{Data: []interface{}{
				1,
				2,
			}}, Response: &abm.TFResponse{
				Data: abm.TFResponseData{Data: 3},
			},
		})

		module.CustomStart(cin, cout)
	}()

	go func() {
		module, err := host.Start(hin, hout, nil, "1.0.0", "")
		if err != nil {
			t.Error(err)
			t.Fail()
		}
		err = module.ExcecuteMethod("templateFunctions_TESTFUNC", []interface{}{}, &struct{}{})
		complete <- err != nil //this should return an error.
	}()

	if <-complete == false {
		println("didnt complete propperly")
		t.Fail()
	}
}

func TestFileLoad(t *testing.T) {
	complete := make(chan bool)

	hin, cout := io.Pipe()
	cin, hout := io.Pipe()

	loadFile := func(w abm.DLRequest, r abm.Response) {
		if w.Variable == "" {
			fmt.Println("invalid input")
			r.AddWarning(abm.InvalidInput)
			return
		}
		r.AddData([]byte("{\"testdata\":\"hey\"}"))
	}

	parseFile := func(w abm.DPRequest, r abm.Response) {
		if w.Data == nil {
			r.AddWarning(abm.InvalidInput)
			return
		}

		var jsonData map[string]interface{}
		err := json.Unmarshal(w.Data, &jsonData)
		if err != nil {
			r.AddFatal(err.Error())
			return
		}
		var retData = make(map[interface{}]interface{})
		for k, v := range jsonData {
			retData[k] = v
		}
		r.AddData(retData)
	}

	processFile := func(w abm.DPPRequest, r abm.Response) {
		if w.Data == nil {
			r.AddWarning(abm.InvalidInput)
			return
		}
		r.AddData(w.Data)
	}

	resultCheck := map[interface{}]interface{}{
		"testdata": "hey",
	}

	go func() {
		module := abm.Register("TEST", "1.0.0")

		module.DataLoaderRegister("file", loadFile)
		module.DataParserRegister("load", parseFile)
		module.DataPostProcessorRegister("process", processFile)

		module.CustomStart(cin, cout)
	}()

	go func() {
		module, err := host.Start(hin, hout, nil, "1.0.0", "")
		if err != nil {
			t.Fail()
		}
		var bytes interface{}
		err = module.ExcecuteMethod("dataLoaders_file", "TEST", &bytes)
		if err != nil {
			complete <- false
		}

		err = module.ExcecuteMethod("dataParsers_load", "n0thing", &bytes, bytes.([]byte)...)
		if err != nil {
			complete <- false
		}

		err = module.ExcecuteMethod("dataPostProcessors_process", "n0thing", &bytes, bytes.([]byte)...)
		if err != nil {
			complete <- false
		}

		var vret tt.Data

		vret.GobDecode(bytes.([]byte))

		for k, v := range vret {
			if v != resultCheck[k] {
				complete <- false
			}
		}

		complete <- err == nil
	}()

	select {
	case succes := <-complete:
		if !succes {
			t.Fail()
		}
	}
}

func TestSiteParse(t *testing.T) {
	complete := make(chan bool)

	hin, cout := io.Pipe()
	cin, hout := io.Pipe()

	resultCheck := []site.Site{
		{
			Data: map[interface{}]interface{}{
				"data": "hey",
				"new":  "yes",
			},
		},
	}
	dataSend := []site.Site{
		{
			Data: map[interface{}]interface{}{
				"data": "hey",
			},
		},
	}

	languageProcess := func(w abm.SPPRequest, r abm.Response) {
		dat := w.Data
		dat[0].Data["new"] = "yes"
		r.AddData(dat)
	}

	go func() {
		module := abm.Register("TEST", "1.0.0")

		module.SitePostProcessorRegister("test", languageProcess)

		module.CustomStart(cin, cout)
	}()

	go func() {
		module, err := host.Start(hin, hout, nil, "1.0.0", "")
		if err != nil {
			t.Fail()
		}
		var bytes []byte
		err = module.ExcecuteMethod("sitePostProcessors_test", "n0thing", &bytes, site.Encode(dataSend)...)
		if err != nil {
			complete <- false
			return
		}

		ret := site.Decode(bytes)

		if err != nil {
			complete <- false
		}

		for k, v := range resultCheck {
			if v.Slug != ret[k].Slug || v.Template != ret[k].Template {
				complete <- false
			}
			for kd, d := range v.Data {
				if d != ret[k].Data[kd] {
					complete <- false
				}
			}
		}
		complete <- true
	}()

	select {
	case succes := <-complete:
		if !succes {
			t.Fail()
		}
	}
}

func startwrap(m *abm.Module, in io.Reader, out io.Writer, ret chan int) {
	m.CustomStart(in, out)
	ret <- 1
}

//benchmark without internal aacceleration
//BenchmarkExecute-8   	  100000	     20714 ns/op	    1001 B/op	      23 allocs/op
//with internal acceleration:
//BenchmarkExecute-8   	  500000	      2316 ns/op	     216 B/op	       5 allocs/op

func BenchmarkExecute(b *testing.B) {
	hin, cout := bufpipe.New([]byte{})
	cin, hout := bufpipe.New([]byte{})

	module := abm.Register("TEST", "1.0.0")

	benchfunc := func(r abm.TFRequest, w abm.Response) {
		w.AddData("hey")
	}
	module.TemplateFunctionRegister("benchfunc", benchfunc, &abm.TFTest{
		Request: abm.TFRequest{
			Data: nil,
		},
		Response: &abm.TFResponse{
			Data: abm.TFResponseData{Data: "hey"},
		},
	})

	go module.CustomStart(cin, cout)
	r := &countreader{Reader: hin}
	w := &countwriter{Writer: hout}
	host, _ := host.Start(r, w, nil, "1.0.0", "")
	for n := 0; n < b.N; n++ {
		host.ExcecuteMethod("templateFunctions_benchfunc", []interface{}{}, &struct{}{})
	}

	//panic(fmt.Sprintf("%d, %d,%d, %d", r.n, w.n, r.n2, w.n2))
}

/*
TT:284, 442,2, 295
GOB:260, 347,9, 9

*/
type countreader struct {
	n  int
	n2 int
	io.Reader
}

func (cr *countreader) Read(p []byte) (int, error) {
	n, err := cr.Reader.Read(p)
	cr.n += n
	cr.n2 += 1
	return n, err
}

type countwriter struct {
	n  int
	n2 int
	io.Writer
}

func (cr *countwriter) Write(p []byte) (int, error) {
	n, err := cr.Writer.Write(p)
	cr.n += n
	cr.n2 += 1
	return n, err
}
