// Copyright © 2018 - 2019 Antipy V.O.F. info@antipy.com
//
// Licensed under the MIT License

package module

import (
	"gitlab.com/antipy/antibuild/api/errors"
	"gitlab.com/antipy/antibuild/api/protocol"
)

/*
	OUTPUT POST PROCESSORS
*/

type (
	//OPPRequest is the request with data and meta from the module caller.
	OPPRequest struct {
		InputFolder  string
		OutputFolder string
		Variable     string
	}

	//OPPResponse is the response to the module API that will be used to respond to the client
	OPPResponse struct {
		Log []errors.Error
	}
	//OutputPostProcessor is a function that processes a site
	OutputPostProcessor func(OPPRequest, Response)

	//outputPostProcessor is a object that stores the site post processor function and its tests.
	outputPostProcessor struct {
		Execute OutputPostProcessor
	}
)

func outputPostProcessorsHandle(command string, r protocol.Token, m *Module) {
	if m.outputPostProcessors[command].Execute == nil {
		r.Error(ErrInvalidCommand)
		return
	}
	var files []string
	err := r.GetData(&files)
	if err != nil {
		r.Error(ErrFailed)
		return
	}

	var request = OPPRequest{
		InputFolder:  files[0],
		OutputFolder: files[1],
	}

	var response = &OPPResponse{}

	m.outputPostProcessors[command].Execute(request, response)

	if r.Respond(nil, response.Log) != nil {
		r.Error(errors.New("failed to send data", errors.CodeInvalidResponse))
	}
}

//OutputPostProcessorRegister registers a new site post processor with specified identifier to the module.
func (m *Module) OutputPostProcessorRegister(identifer string, function OutputPostProcessor) {
	outputPostProcessorRegister(m, identifer, function)
}

func outputPostProcessorRegister(m *Module, identifer string, function OutputPostProcessor) {
	if identifer == "" {
		panic("module: outputPostProcessor: identifer is not defined")
	}

	if m.outputPostProcessors == nil {
		panic("module: outputPostProcessor: initalization of module was not correct")
	}

	if _, ok := m.outputPostProcessors[identifer]; ok {
		panic("module: outputPostProcessor: outputPostProcessor with this identifier is already registered")
	}

	if function == nil {
		panic("module: outputPostProcessor: function is not defined")
	}

	m.outputPostProcessors[identifer] = outputPostProcessor{
		Execute: function,
	}
}

//AddDebug adds a debug message to the log
func (spp *OPPResponse) AddDebug(message string) {
	spp.Log = append(spp.Log, errors.New(message, errors.CodeDebug))
}

//AddInfo adds an info message to the log
func (spp *OPPResponse) AddInfo(message string) {
	spp.Log = append(spp.Log, errors.New(message, errors.CodeInfo))
}

//AddWarning adds an error in the log
func (spp *OPPResponse) AddWarning(message string) {
	spp.Log = append(spp.Log, errors.New(message, errors.CodeWarning))
}

//AddFatal adds a fatal error to the log
func (spp *OPPResponse) AddFatal(message string) {
	spp.Log = append(spp.Log, errors.New(message, errors.CodeFatal))
}

//AddData adds the data to the response
func (spp *OPPResponse) AddData(data interface{}) bool {
	spp.AddFatal("outputPostProcessors are not allowed to respond with data")
	return true
}
