// Copyright © 2018 - 2019 Antipy V.O.F. info@antipy.com
//
// Licensed under the MIT License

package module

import (
	"reflect"

	"gitlab.com/antipy/antibuild/api/errors"
	"gitlab.com/antipy/antibuild/api/protocol"
)

/*
	DATA LOADERS
*/

type (
	//DLRequest is the request with data and meta from the module caller.
	DLRequest struct {
		Variable string
	}

	//DLResponse is the response to the module API that will be used to respond to the client
	DLResponse struct {
		Log  []errors.Error
		data []byte
	}

	//DLTest is a object that is used to test a file loader.
	DLTest struct {
		Request  DLRequest
		Response *DLResponse
	}
	//DataLoader is a function that loads files
	DataLoader func(DLRequest, Response)

	//dataLoader is a object that stores a DataLoader and tests.
	dataLoader struct {
		Execute DataLoader
	}
)

func dataLoadersHandle(command string, r protocol.Token, m *Module) {
	if m.dataLoaders[command].Execute == nil {
		r.Error(ErrInvalidCommand)
		return
	}
	var variable string
	err := r.GetData(&variable)
	if err != nil {
		r.Error(ErrInvalidInput)
		return
	}

	var request = DLRequest{
		Variable: variable,
	}

	var response = &DLResponse{}

	m.dataLoaders[command].Execute(request, response)

	if r.Respond(response.data, response.Log) != nil {
		r.Error(errors.New("failed to send data", errors.CodeInvalidResponse))
	}
}

//DataLoaderRegister registers a new file loader with specified identifier to the module.
func (m *Module) DataLoaderRegister(identifer string, function DataLoader) {
	dataLoaderRegister(m, identifer, function)
}

func dataLoaderRegister(m *Module, identifer string, function DataLoader) {
	if identifer == "" {
		panic("module: dataLoaderRegister: identifer is not defined")
	}

	if m.dataLoaders == nil {
		panic("module: dataLoaderRegister: initalization of module was not correct")
	}

	if _, ok := m.dataLoaders[identifer]; ok {
		panic("module: dataLoaderRegister: dataLoader with this identifier is already registered")

	}

	if function == nil {
		panic("module: dataLoaderRegister: function is not defined")
	}

	m.dataLoaders[identifer] = dataLoader{
		Execute: function,
	}
}

//AddDebug adds a debug message to the log
func (dl *DLResponse) AddDebug(message string) {
	dl.Log = append(dl.Log, errors.New(message, errors.CodeDebug))
}

//AddInfo adds an info message to the log
func (dl *DLResponse) AddInfo(message string) {
	dl.Log = append(dl.Log, errors.New(message, errors.CodeInfo))
}

//AddWarning adds an error in the log
func (dl *DLResponse) AddWarning(message string) {
	dl.Log = append(dl.Log, errors.New(message, errors.CodeWarning))
}

//AddFatal adds a fatal error to the log
func (dl *DLResponse) AddFatal(message string) {
	dl.Log = append(dl.Log, errors.New(message, errors.CodeFatal))
}

//AddData adds the data to the response
func (dl *DLResponse) AddData(data interface{}) bool {
	if _, ok := data.([]byte); !ok {
		dl.AddFatal("return data is not valid, should be []byte but is:" + reflect.TypeOf(data).String())

		return false
	}
	dl.data = data.([]byte)
	return true
}
