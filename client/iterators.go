// Copyright © 2018 - 2019 Antipy V.O.F. info@antipy.com
//
// Licensed under the MIT License

package module

import (
	"reflect"

	"gitlab.com/antipy/antibuild/api/errors"
	"gitlab.com/antipy/antibuild/api/protocol"
)

/*
	ITERATORS
*/

type (
	//ITRequest is the request with data and meta from the module caller.
	ITRequest struct {
		Variable string
	}

	//ITResponse is the response to the module API that will be used to respond to the client
	ITResponse struct {
		Log  []errors.Error
		Data []string
	}

	//ITTest is a object that is used to test an iterator.
	ITTest struct {
		Request  ITRequest
		Response *ITResponse
	}
	//Iterator is a function that loads files
	Iterator func(ITRequest, Response)

	//iterator is a object that stores a Iterator and tests.
	iterator struct {
		Execute Iterator
	}
)

func iteratorsHandle(command string, r protocol.Token, m *Module) {
	if m.iterators[command].Execute == nil {
		r.Error(ErrInvalidCommand)
		return
	}

	var variable string
	err := r.GetData(&variable)
	if err != nil {
		r.Error(ErrInvalidInput)
		return
	}

	var request = ITRequest{
		Variable: variable,
	}

	var response = &ITResponse{}

	m.iterators[command].Execute(request, response)

	if r.Respond(response.Data, response.Log) != nil {
		r.Error(errors.New("failed to send data", errors.CodeInvalidResponse))
	}
}

//IteratorRegister registers a new iterator with specified identifier to the module.
func (m *Module) IteratorRegister(identifer string, function Iterator) {
	iteratorRegister(m, identifer, function)
}

func iteratorRegister(m *Module, identifer string, function Iterator) {
	if identifer == "" {
		panic("module: iteratorRegister: identifer is not defined")
	}

	if m.iterators == nil {
		panic("module: iteratorRegister: initalization of module was not correct")
	}

	if _, ok := m.iterators[identifer]; ok {
		panic("module: iteratorRegister: iterator with this identifier is already registered")

	}

	if function == nil {
		panic("module: iteratorRegister: function is not defined")
	}

	m.iterators[identifer] = iterator{
		Execute: function,
	}
}

//AddDebug adds a debug message to the log
func (it *ITResponse) AddDebug(message string) {
	it.Log = append(it.Log, errors.New(message, errors.CodeDebug))
}

//AddInfo adds an info message to the log
func (it *ITResponse) AddInfo(message string) {
	it.Log = append(it.Log, errors.New(message, errors.CodeInfo))
}

//AddWarning adds an error in the log
func (it *ITResponse) AddWarning(message string) {
	it.Log = append(it.Log, errors.New(message, errors.CodeWarning))
}

//AddFatal adds a fatal error to the log
func (it *ITResponse) AddFatal(message string) {
	it.Log = append(it.Log, errors.New(message, errors.CodeFatal))
}

//AddData adds the data to the response
func (it *ITResponse) AddData(data interface{}) bool {
	if _, ok := data.([]string); !ok {
		it.AddFatal("return data is not valid, should be []string but is:" + reflect.TypeOf(data).String())

		return false
	}
	it.Data = data.([]string)
	return true
}
