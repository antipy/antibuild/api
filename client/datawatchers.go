// Copyright © 2018 - 2019 Antipy V.O.F. info@antipy.com
//
// Licensed under the MIT License

package module

import (
	"os"
	"reflect"

	"gitlab.com/antipy/antibuild/api/errors"
	"gitlab.com/antipy/antibuild/api/protocol"
)

/*
	DATA LOADERS
*/

type (
	//drRequest is the request with data and meta from the module caller.
	DWRequest struct {
		Variables []string
	}

	//drResponse is the response to the module API that will be used to respond to the client
	DWResponse struct {
		prefix string
		Log    []errors.Error
		r      protocol.Token
	}

	//drTest is a object that is used to test a file loader.
	DWTest struct {
		Request  DWRequest
		Response *DWResponse
	}

	//DataLoader is a function that loads files
	DataWatcher func(DWRequest, Response)

	//dataLoader is a object that stores a DataLoader and tests.
	dataWatcher struct {
		oldResponse *DWResponse
		Execute     DataWatcher
	}
)

func dataWatchersHandle(command string, r protocol.Token, m *Module) {
	if m.dataLoaders[command].Execute == nil {
		r.Error(ErrInvalidCommand)
		return
	}

	var variable []string
	err := r.GetData(&variable)
	if err != nil {
		r.Error(ErrInvalidInput)
		return
	}

	var request = DWRequest{
		Variables: variable,
	}
	if m.dataWatchers[command].oldResponse != nil {
		m.dataWatchers[command].oldResponse.r.Respond("KILL", nil)
	}

	var response = &DWResponse{r: r, prefix: m.name + "_" + command + ":"}

	dwd := m.dataWatchers[command]
	dwd.oldResponse = response
	m.dataWatchers[command] = dwd

	m.dataWatchers[command].Execute(request, response)
}

//DataLoaderRegister registers a new file loader with specified identifier to the module.
func (m *Module) DataWatcherRegister(identifer string, function DataWatcher) {
	dataWatcherRegister(m, identifer, function)
}

func dataWatcherRegister(m *Module, identifer string, function DataWatcher) {
	if identifer == "" {
		panic("module: dataLoaderRegister: identifer is not defined")
	}

	if m.dataWatchers == nil {
		panic("module: dataLoaderRegister: initalization of module was not correct")
	}

	if _, ok := m.dataWatchers[identifer]; ok {
		panic("module: dataLoaderRegister: dataLoader with this identifier is already registered")
	}

	if function == nil {
		panic("module: dataLoaderRegister: function is not defined")
	}

	m.dataWatchers[identifer] = dataWatcher{
		Execute: function,
	}
}

//AddDebug adds a debug message to the log
func (dr *DWResponse) AddDebug(message string) {
	dr.Log = append(dr.Log, errors.New(message, errors.CodeDebug))
}

//AddInfo adds an info message to the log
func (dr *DWResponse) AddInfo(message string) {
	dr.Log = append(dr.Log, errors.New(message, errors.CodeInfo))
}

//AddWarning adds an error in the log
func (dr *DWResponse) AddWarning(message string) {
	dr.Log = append(dr.Log, errors.New(message, errors.CodeWarning))
}

//AddFatal adds a fatal error to the log
func (dr *DWResponse) AddFatal(message string) {
	dr.Log = append(dr.Log, errors.New(message, errors.CodeFatal))
}

//AddData adds the data to the response
func (dr *DWResponse) AddData(data interface{}) bool {
	f, _ := os.Create("BB")
	if data, ok := data.(string); ok {
		if dr.r.Respond(dr.prefix+data, dr.Log) != nil {
			f.WriteString("failed to send data")
			dr.r.Error(errors.New("failed to send data", errors.CodeInvalidResponse))
			return false
		}

		return true
	}
	f.WriteString("return data is not valid, should be string but is:" + reflect.TypeOf(data).String())
	dr.AddFatal("return data is not valid, should be string but is:" + reflect.TypeOf(data).String())
	return false
}
