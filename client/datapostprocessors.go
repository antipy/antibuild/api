// Copyright © 2018 - 2019 Antipy V.O.F. info@antipy.com
//
// Licensed under the MIT License

package module

import (
	"bytes"
	"reflect"

	"github.com/jaicewizard/tt"
	"gitlab.com/antipy/antibuild/api/errors"
	"gitlab.com/antipy/antibuild/api/protocol"
)

/*
	DATA POST PROCESSORS
*/
type (
	//DPPRequest is the request with data and meta from the module caller.
	DPPRequest struct {
		Data     map[interface{}]interface{}
		Variable string
	}

	//DPPResponse is the response to the module API that will be used to respond to the client
	DPPResponse struct {
		Log  []errors.Error
		data []byte
	}

	//DPPTest is a object that is used to test a data post processor.
	DPPTest struct {
		Request  DPPRequest
		Response *DPPResponse
	}

	//DataPostProcessor is a function that processes some data
	DataPostProcessor func(DPPRequest, Response)

	//dataPostProcessor is a object that stores the data post processor function and its tests.
	dataPostProcessor struct {
		Execute DataPostProcessor
	}
)

func dataPostProcessorsHandle(command string, r protocol.Token, m *Module) {
	if m.dataPostProcessors[command].Execute == nil {
		r.Error(ErrInvalidCommand)
		return
	}
	var objectInput map[interface{}]interface{}
	buf := bytes.NewBuffer(r.Bin)
	err := tt.Decodev3(buf, &objectInput)
	if err != nil {
		r.Error(ErrFailed)
		return
	}

	var variable string
	err = r.GetData(&variable)
	if err != nil {
		r.Error(ErrInvalidInput)
		return
	}

	var request = DPPRequest{
		Data:     objectInput,
		Variable: variable,
	}

	var response = &DPPResponse{}

	m.dataPostProcessors[command].Execute(request, response)

	if r.Respond(response.data, response.Log) != nil {
		r.Error(errors.New("failed to send data", errors.CodeInvalidResponse))
	}
}

//DataPostProcessorRegister registers a new file post processor with specified identifier to the module.
func (m *Module) DataPostProcessorRegister(identifer string, function DataPostProcessor) {
	dataPostProcessorRegister(m, identifer, function)
}

func dataPostProcessorRegister(m *Module, identifer string, function DataPostProcessor) {
	if identifer == "" {
		panic("module: dataPostProcessor: identifer is not defined")
	}

	if m.dataPostProcessors == nil {
		panic("module: dataPostProcessor: initalization of module was not correct")
	}

	if _, ok := m.dataPostProcessors[identifer]; ok {
		panic("module: dataPostProcessor: dataPostProcessor with this identifier is already registered")
	}

	if function == nil {
		panic("module: dataPostProcessor: function is not defined")
	}

	m.dataPostProcessors[identifer] = dataPostProcessor{
		Execute: function,
	}
}

//AddDebug adds a debug message to the log
func (dpp *DPPResponse) AddDebug(message string) {
	dpp.Log = append(dpp.Log, errors.New(message, errors.CodeDebug))
}

//AddInfo adds an info message to the log
func (dpp *DPPResponse) AddInfo(message string) {
	dpp.Log = append(dpp.Log, errors.New(message, errors.CodeInfo))
}

//AddWarning adds an error in the log
func (dpp *DPPResponse) AddWarning(message string) {
	dpp.Log = append(dpp.Log, errors.New(message, errors.CodeWarning))
}

//AddFatal adds a fatal error to the log
func (dpp *DPPResponse) AddFatal(message string) {
	dpp.Log = append(dpp.Log, errors.New(message, errors.CodeFatal))
}

//AddData adds the data to the response
func (dpp *DPPResponse) AddData(data interface{}) bool {
	var d tt.Data
	var ok bool
	var err error
	if d, ok = data.(map[interface{}]interface{}); !ok { //not tt.Data since it is not actually the type so that will fail
		dpp.AddFatal("return data is not valid, should be map[interface{}]interface{ but is:" + reflect.TypeOf(data).String())
		return false
	}

	dpp.data, err = d.GobEncode()
	if err != nil {
		dpp.AddFatal("could not encode Data")
	}

	return true
}
