// Copyright © 2018 - 2019 Antipy V.O.F. info@antipy.com
//
// Licensed under the MIT License

package module

import (
	"io"
	"os"
	"strings"

	"gitlab.com/antipy/antibuild/api/errors"
	"gitlab.com/antipy/antibuild/api/internal"
	"gitlab.com/antipy/antibuild/api/protocol"
)

type (
	//Module is the collection of registered events that the module API should react to.
	Module struct {
		name    string
		version string

		configFunction func(map[string]interface{}) *errors.Error

		templateFunctions    map[string]templateFunction
		dataLoaders          map[string]dataLoader
		dataWatchers         map[string]dataWatcher
		dataParsers          map[string]dataParser
		dataPostProcessors   map[string]dataPostProcessor
		sitePostProcessors   map[string]sitePostProcessor
		iterators            map[string]iterator
		outputPostProcessors map[string]outputPostProcessor

		finalizer func()
	}
	//Response is a response to a request
	Response interface {
		AddDebug(message string)
		AddInfo(message string)
		AddWarning(message string)
		AddFatal(message string)
		AddData(data interface{}) bool
	}
)

const (
	//keys in the config map
	keyTemplateFunctions    = "templateFunctions"
	keyDataLoaders          = "dataLoaders"
	keyDataWatchers         = "dataWatchers"
	keyDataParsers          = "dataParsers"
	keyDataPostProcessors   = "dataPostProcessors"
	keySitePostProcessors   = "sitePostProcessors"
	keyIterators            = "iterators"
	keyOutputPostProcessors = "outputPostProcessors"

	//InvalidCommand is the errors that occurs when a function is called that is not registered with the module.
	InvalidCommand = "module: the provided command does not exist"

	//InvalidInput is the errors that occurs when a function is called with data that is not correct, valid or applicable.
	InvalidInput = "module: the provided data is invalid"

	//Failed is the errors that occurs when the module experiences an internal errors.
	Failed = "module: internal processing errors"

	//NoConfig is the errors that occurs when the module has not recieved its configuration when needed.
	NoConfig = "module: config has not been recieved yet"

	//ModuleReady tells the host that the config worked
	ModuleReady = "module: ready"
)

var (
	//ErrInvalidCommand is the errors that occurs when a function is called that is not registered with the module.
	ErrInvalidCommand = errors.New(InvalidCommand, errors.CodeFatal)

	//ErrInvalidInput is the errors that occurs when a function is called with data that is not correct, valid or applicable.
	ErrInvalidInput = errors.New(InvalidInput, errors.CodeFatal)

	//ErrFailed is the errors that occurs when the module experiences an internal errors.
	ErrFailed = errors.New(Failed, errors.CodeFatal)

	//ErrNoConfig is the errors that occurs when the module has not recieved its configuration when needed.
	ErrNoConfig = errors.New(NoConfig, errors.CodeFatal)
)

/*
	Module Management Functions
*/

//Register registers a new module with its meta information.
func Register(name string, version string) (module *Module) {
	return register(name, version)
}

func register(name string, version string) *Module {
	module := &Module{}

	if name == "" {
		panic("module: name is not defined")
	}

	if version == "" {
		panic("module: version is not defined")
	}

	module.name = name
	module.version = version

	module.templateFunctions = make(map[string]templateFunction)
	module.dataLoaders = make(map[string]dataLoader)
	module.dataWatchers = make(map[string]dataWatcher)
	module.dataParsers = make(map[string]dataParser)
	module.dataPostProcessors = make(map[string]dataPostProcessor)
	module.sitePostProcessors = make(map[string]sitePostProcessor)
	module.iterators = make(map[string]iterator)
	module.outputPostProcessors = make(map[string]outputPostProcessor)

	return module
}

/*
	Module Start
*/

//Start listenes to messages from host and responds if possible. Should be called AFTER registering all functions to the module.
func (m *Module) Start() {
	start(m, os.Stdin, os.Stdout)
}

//CustomStart just like Start but with custom read adn writers
func (m *Module) CustomStart(in io.Reader, out io.Writer) {
	start(m, in, out)
}

func start(m *Module, in io.Reader, out io.Writer) {
	if m.finalizer != nil {
		defer m.finalizer()
	}
	con := protocol.OpenConnection(in, out)
	con.ID = m.name
	version, err := con.Init(false, m.version)

	if err != nil {
		panic(err)
	}
	if version.HasSamePID() {
		con.SetSendReceiver(internal.AddLoadedModules(version.Module))
	}

	for {
		t, err := con.Receive()
		if err == protocol.ErrEOF {
			break
		}
		if t.Command == protocol.KillCommand {
			t.Respond(nil, nil)
			return
		} else if t.Command == protocol.GetMethods {
			getMethodsHandle(t, m)
			continue
		}
		commandSplit := strings.SplitN(t.Command, "_", 2)
		switch commandSplit[0] {
		case "internal":
			internalHandle(commandSplit[1], t, m)
		case keyTemplateFunctions:
			templateFunctionsHandle(commandSplit[1], t, m)
		case keyDataLoaders:
			dataLoadersHandle(commandSplit[1], t, m)
		case keyDataWatchers:
			dataWatchersHandle(commandSplit[1], t, m)
		case keyDataParsers:
			dataParsersHandle(commandSplit[1], t, m)
		case keyDataPostProcessors:
			dataPostProcessorsHandle(commandSplit[1], t, m)
		case keySitePostProcessors:
			sitePostProcessorsHandle(commandSplit[1], t, m)
		case keyIterators:
			iteratorsHandle(commandSplit[1], t, m)
		case keyOutputPostProcessors:
			outputPostProcessorsHandle(commandSplit[1], t, m)
		default:
			t.Error(ErrInvalidCommand)
		}
	}
}

func getMethodsHandle(t protocol.Token, m *Module) {
	var (
		templateFunctions    = make([]string, 0, len(m.templateFunctions))
		dataLoaders          = make([]string, 0, len(m.dataLoaders))
		dataWatchers         = make([]string, 0, len(m.dataWatchers))
		dataParsers          = make([]string, 0, len(m.dataParsers))
		dataPostProcessors   = make([]string, 0, len(m.dataPostProcessors))
		sitePostProcessors   = make([]string, 0, len(m.sitePostProcessors))
		iterators            = make([]string, 0, len(m.iterators))
		outputPostProcessors = make([]string, 0, len(m.outputPostProcessors))
	)

	for key := range m.templateFunctions {
		templateFunctions = append(templateFunctions, key)
	}

	for key := range m.dataLoaders {
		dataLoaders = append(dataLoaders, key)
	}

	for key := range m.dataWatchers {
		dataWatchers = append(dataWatchers, key)
	}

	for key := range m.dataParsers {
		dataParsers = append(dataParsers, key)
	}

	for key := range m.dataPostProcessors {
		dataPostProcessors = append(dataPostProcessors, key)
	}

	for key := range m.sitePostProcessors {
		sitePostProcessors = append(sitePostProcessors, key)
	}

	for key := range m.iterators {
		iterators = append(iterators, key)
	}
	for key := range m.outputPostProcessors {
		outputPostProcessors = append(outputPostProcessors, key)
	}

	t.Respond(protocol.Methods{
		keyTemplateFunctions:    templateFunctions,
		keyDataLoaders:          dataLoaders,
		keyDataWatchers:         dataWatchers,
		keyDataParsers:          dataParsers,
		keyDataPostProcessors:   dataPostProcessors,
		keySitePostProcessors:   sitePostProcessors,
		keyIterators:            iterators,
		keyOutputPostProcessors: outputPostProcessors,
	}, nil)
}

func internalHandle(command string, t protocol.Token, m *Module) {
	switch command {
	case "config":
		var objectInput map[string]interface{}
		err := t.GetData(&objectInput)
		if err != nil {
			t.Error(ErrInvalidInput)
			return
		}
		if m.configFunction == nil {
			t.Succes(ModuleReady)
			return
		}

		if err := m.configFunction(objectInput); err != nil {
			t.Error(*err)
			return
		}

		t.Succes(ModuleReady)
	case "testMethods":
		t.Succes(testMethods(m))
	}
}

func testMethods(m *Module) bool {
	for _, templateFunction := range m.templateFunctions {
		var response = &TFResponse{}

		templateFunction.Execute(templateFunction.Test.Request, response)

		for _, log := range response.Log {
			if log.Code == errors.CodeFatal {
				return false
			}
		}

		if response.Data != templateFunction.Test.Response.Data {
			return false
		}
	}

	return true
}

//ConfigFunctionRegister is the function that will be called that handles the config of the client.
func (m *Module) ConfigFunctionRegister(function func(map[string]interface{}) *errors.Error) {
	configFunctionRegister(m, function)
}

func configFunctionRegister(m *Module, function func(map[string]interface{}) *errors.Error) {
	if function == nil {
		panic("module: configFunctionRegister: function is not defined")
	}

	m.configFunction = function
}

//FinalizerFunctionRegister registers the function that will be called whenever
//the module shuts down.
func (m *Module) FinalizerFunctionRegister(function func()) {
	finalizerFunctionRegister(m, function)
}

func finalizerFunctionRegister(m *Module, function func()) {
	if function == nil {
		panic("module: configFunctionRegister: function is not defined")
	}

	m.finalizer = function
}
