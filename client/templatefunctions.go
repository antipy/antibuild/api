// Copyright © 2018 - 2019 Antipy V.O.F. info@antipy.com
//
// Licensed under the MIT License

package module

import (
	"html/template"

	"gitlab.com/antipy/antibuild/api/errors"
	"gitlab.com/antipy/antibuild/api/protocol"
)

type (

	//TFRequest is the request with data and meta from the module caller.
	TFRequest struct {
		Data []interface{}
	}

	//TFResponse is the response to the module API that will be used to respond to the client
	TFResponse struct {
		Data TFResponseData
		Log  []errors.Error
	}

	//TFResponseData is the response to the module API that will be used to respond to the client
	TFResponseData struct {
		IsHTMLEscaped     bool
		IsHTMLAttrEscaped bool
		IsJSEscaped       bool
		IsJSStrEscaped    bool
		IsCSSEscaped      bool
		Data              interface{}
	}

	//TFTest is a object that is used to test a function.
	TFTest struct {
		Request  TFRequest
		Response *TFResponse
	}

	//TemplateFunction is a function thats usable in golang html templates
	TemplateFunction func(TFRequest, Response)

	//templateFunction is a object that stores the template function and its tests.
	templateFunction struct {
		Execute TemplateFunction
		Test    *TFTest
	}
)

//TemplateFunctionRegister registers a new template function with specified identifier to the module.
func (m *Module) TemplateFunctionRegister(identifer string, function TemplateFunction, test *TFTest) {
	templateFunctionRegister(m, identifer, function, test)
}

func templateFunctionRegister(m *Module, identifer string, function TemplateFunction, test *TFTest) {
	if identifer == "" {
		panic("module: templateFunctionRegister: identifer is not defined")
	}

	if _, ok := m.templateFunctions[identifer]; ok {
		panic("module: templateFunctionRegister: TemplateFunction with this identifier is already registered")
	}

	if function == nil {
		panic("module: templateFunctionRegister: function is not defined")
	}

	if test == nil {
		panic("module: templateFunctionRegister: test is not defined")
	}

	if test.Response.Data.Data == nil {
		panic("module: templateFunctionRegister: test response data is not defined")
	}

	m.templateFunctions[identifer] = templateFunction{
		Execute: function,
		Test:    test,
	}
}

func templateFunctionsHandle(command string, r protocol.Token, m *Module) {
	if m.templateFunctions[command].Execute == nil {
		r.Error(ErrInvalidCommand)
		return
	}
	var data []interface{}
	err := r.GetData(&data)
	if err != nil {
		r.Error(ErrInvalidInput)
		return
	}

	var request = TFRequest{
		Data: data,
	}

	var response = &TFResponse{}

	m.templateFunctions[command].Execute(request, response)

	if err := r.Respond(response.Data, response.Log); err != nil {
		r.Error(errors.New("failed to send data: "+err.Error(), errors.CodeInvalidResponse))
	}
}

//AddDebug adds a debug message to the log
func (tf *TFResponse) AddDebug(message string) {
	tf.Log = append(tf.Log, errors.New(message, errors.CodeDebug))
}

//AddInfo adds an info message to the log
func (tf *TFResponse) AddInfo(message string) {
	tf.Log = append(tf.Log, errors.New(message, errors.CodeInfo))
}

//AddWarning adds an error in the log
func (tf *TFResponse) AddWarning(message string) {
	tf.Log = append(tf.Log, errors.New(message, errors.CodeWarning))
}

//AddFatal adds a fatal error to the log
func (tf *TFResponse) AddFatal(message string) {
	tf.Log = append(tf.Log, errors.New(message, errors.CodeFatal))
}

//AddData adds the data to the response
func (tf *TFResponse) AddData(data interface{}) bool {
	if _, ok := data.(template.HTML); ok {
		tf.Data.IsHTMLEscaped = true
	}
	if _, ok := data.(template.HTMLAttr); ok {
		tf.Data.IsHTMLEscaped = true
	}
	if _, ok := data.(template.JS); ok {
		tf.Data.IsJSEscaped = true
	}
	if _, ok := data.(template.JSStr); ok {
		tf.Data.IsJSStrEscaped = true
	}
	if _, ok := data.(template.CSS); ok {
		tf.Data.IsCSSEscaped = true
	}
	tf.Data.Data = data
	return true
}
