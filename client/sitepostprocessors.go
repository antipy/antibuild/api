// Copyright © 2018 - 2019 Antipy V.O.F. info@antipy.com
//
// Licensed under the MIT License

package module

import (
	"reflect"

	"gitlab.com/antipy/antibuild/api/errors"
	"gitlab.com/antipy/antibuild/api/protocol"
	"gitlab.com/antipy/antibuild/api/site"
)

/*
	SITE POST PROCESSORS
*/

type (

	//SPPRequest is the request with data and meta from the module caller.
	SPPRequest struct {
		Data     []site.Site
		Variable string
	}

	//SPPResponse is the response to the module API that will be used to respond to the client
	SPPResponse struct {
		Log  []errors.Error
		data []byte
	}
	//SitePortProcessor is a function that processes a site
	SitePortProcessor func(SPPRequest, Response)

	//sitePostProcessor is a object that stores the site post processor function and its tests.
	sitePostProcessor struct {
		Execute SitePortProcessor
	}
)

func sitePostProcessorsHandle(command string, r protocol.Token, m *Module) {
	if m.sitePostProcessors[command].Execute == nil {
		r.Error(ErrInvalidCommand)
		return
	}

	objectInput := site.Decode(r.Bin)

	var variable string
	err := r.GetData(&variable)
	if err != nil {
		r.Error(ErrInvalidInput)
		return
	}

	var request = SPPRequest{
		Data:     objectInput,
		Variable: variable,
	}

	var response = &SPPResponse{}

	m.sitePostProcessors[command].Execute(request, response)

	if r.Respond(response.data, response.Log) != nil {
		r.Error(errors.New("failed to send data", errors.CodeInvalidResponse))
	}
}

//SitePostProcessorRegister registers a new site post processor with specified identifier to the module.
func (m *Module) SitePostProcessorRegister(identifer string, function SitePortProcessor) {
	sitePostProcessorRegister(m, identifer, function)
}

func sitePostProcessorRegister(m *Module, identifer string, function SitePortProcessor) {
	if identifer == "" {
		panic("module: sitePostProcessor: identifer is not defined")
	}

	if m.sitePostProcessors == nil {
		panic("module: sitePostProcessor: initalization of module was not correct")
	}

	if _, ok := m.sitePostProcessors[identifer]; ok {
		panic("module: sitePostProcessor: sitePostProcessor with this identifier is already registered")
	}

	if function == nil {
		panic("module: sitePostProcessor: function is not defined")
	}

	m.sitePostProcessors[identifer] = sitePostProcessor{
		Execute: function,
	}
}

//AddDebug adds a debug message to the log
func (spp *SPPResponse) AddDebug(message string) {
	spp.Log = append(spp.Log, errors.New(message, errors.CodeDebug))
}

//AddInfo adds an info message to the log
func (spp *SPPResponse) AddInfo(message string) {
	spp.Log = append(spp.Log, errors.New(message, errors.CodeInfo))
}

//AddWarning adds an error in the log
func (spp *SPPResponse) AddWarning(message string) {
	spp.Log = append(spp.Log, errors.New(message, errors.CodeWarning))
}

//AddFatal adds a fatal error to the log
func (spp *SPPResponse) AddFatal(message string) {
	spp.Log = append(spp.Log, errors.New(message, errors.CodeFatal))
}

//AddData adds the data to the response
func (spp *SPPResponse) AddData(data interface{}) bool {
	var v []site.Site
	var ok bool
	if v, ok = data.([]site.Site); !ok {
		spp.AddFatal("return data is not valid, should be []site.Site but is:" + reflect.TypeOf(data).String())
		return false
	}
	spp.data = site.Encode(v)
	return true
}
