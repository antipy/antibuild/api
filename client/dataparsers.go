// Copyright © 2018 - 2019 Antipy V.O.F. info@antipy.com
//
// Licensed under the MIT License

package module

import (
	"reflect"

	"github.com/jaicewizard/tt"
	"gitlab.com/antipy/antibuild/api/errors"
	"gitlab.com/antipy/antibuild/api/protocol"
)

/*
	FILE PARSERS
*/

type (

	//DPRequest is the request with data and meta from the module caller.
	DPRequest struct {
		Data     []byte
		Variable string
	}

	//DPResponse is the response to the module API that will be used to respond to the client
	DPResponse struct {
		Log  []errors.Error
		data []byte
	}

	//DPTest is a object that is used to test a data parser.
	DPTest struct {
		Request  DPRequest
		Response *DPResponse
	}

	//DataParser is a function that is able to parse data
	DataParser func(DPRequest, Response)

	//dataParser is a object that stores a DataParser and its tests.
	dataParser struct {
		Execute DataParser
	}
)

func dataParsersHandle(command string, r protocol.Token, m *Module) {
	if m.dataParsers[command].Execute == nil {
		r.Error(ErrInvalidCommand)
		return
	}

	var variable string
	err := r.GetData(&variable)
	if err != nil {
		r.Error(ErrInvalidInput)
		return
	}

	var request = DPRequest{
		Data:     r.Bin,
		Variable: variable,
	}

	var response = &DPResponse{}

	m.dataParsers[command].Execute(request, response)

	if r.Respond(response.data, response.Log) != nil {
		r.Error(errors.New("failed to send data", errors.CodeInvalidResponse))
	}
}

//DataParserRegister registers a new file parser with specified identifier to the module.
func (m *Module) DataParserRegister(identifer string, function DataParser) {
	dataParserRegister(m, identifer, function)
}

func dataParserRegister(m *Module, identifer string, function DataParser) {
	if identifer == "" {
		panic("module: dataParserRegister: identifer is not defined")
	}

	if m.dataParsers == nil {
		panic("module: dataParserRegister: initalization of module was not correct")
	}

	if _, ok := m.dataParsers[identifer]; ok {
		panic("module: dataParserRegister: dataParser with this identifier is already registered")
	}

	if function == nil {
		panic("module: dataParserRegister: function is not defined")
	}

	m.dataParsers[identifer] = dataParser{
		Execute: function,
	}
}

//AddDebug adds a debug message to the log
func (dp *DPResponse) AddDebug(message string) {
	dp.Log = append(dp.Log, errors.New(message, errors.CodeDebug))
}

//AddInfo adds an info message to the log
func (dp *DPResponse) AddInfo(message string) {
	dp.Log = append(dp.Log, errors.New(message, errors.CodeInfo))
}

//AddWarning adds an error in the log
func (dp *DPResponse) AddWarning(message string) {
	dp.Log = append(dp.Log, errors.New(message, errors.CodeWarning))
}

//AddFatal adds a fatal error to the log
func (dp *DPResponse) AddFatal(message string) {
	dp.Log = append(dp.Log, errors.New(message, errors.CodeFatal))
}

//AddData adds the data to the response
func (dp *DPResponse) AddData(data interface{}) bool {
	var d tt.Data
	var ok bool
	var err error
	if d, ok = data.(map[interface{}]interface{}); !ok { //not tt.Data since it is not actually the type so that will fail
		dp.AddFatal("return data is not valid, should be map[interface{}]interface{ but is:" + reflect.TypeOf(data).String())
		return false
	}

	dp.data, err = d.GobEncode()
	if err != nil {
		dp.AddFatal("could not encode Data")
	}
	return true
}
