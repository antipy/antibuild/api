// Copyright © 2018 - 2019 Antipy V.O.F. info@antipy.com
//
// Licensed under the MIT License

package host

import (
	"io"
	"math/rand"
	"reflect"
	"sync"

	"gitlab.com/antipy/antibuild/api/errors"
	"gitlab.com/antipy/antibuild/api/internal"
	"gitlab.com/antipy/antibuild/api/protocol"
)

type (
	response struct {
		resp protocol.Response
		err  error
	}
	command struct {
		send chan response
		ret  interface{}
	}

	//ModuleHost is the host for a module with all the data
	ModuleHost struct {
		commands map[protocol.ID]*command
		lock     sync.RWMutex
		con      *protocol.Connection
		Name     string
		Logger
	}

	//Logger is a simple logger only used for debug output
	Logger interface {
		Fatal(string)
		Fatalf(string, ...interface{})
		Warning(string)
		Warningf(string, ...interface{})
		Info(string)
		Infof(string, ...interface{})
		Debug(string)
		Debugf(string, ...interface{})
	}

	logger struct{}
)

var (
	//ErrFailedToGenID is for when it failed generating an ID
	ErrFailedToGenID = errors.New("could not generate random ID", errors.CodeFatal)
	//ErrInvalidResponse is for when the response is of a wrong type
	ErrInvalidResponse = errors.New("return datatype is incorrect", errors.CodeInvalidResponse)
)

//Start starts the Initites protocol for a given io.Reader and io.Writer.
func Start(in io.Reader, out io.Writer, log Logger, expectedVersion, moduleName string) (moduleHost *ModuleHost, err error) {
	moduleHost = &ModuleHost{}

	moduleHost.lock = sync.RWMutex{}
	moduleHost.commands = make(map[protocol.ID]*command)
	moduleHost.con = protocol.OpenConnection(in, out)
	moduleHost.Name = moduleName
	moduleHost.Logger = log

	if moduleHost.Logger == nil {
		moduleHost.Logger = logger{}
	}

	version, err := moduleHost.con.Init(true, expectedVersion)
	if err != nil {
		return
	}
	if version.HasSamePID() {
		moduleHost.con.SetSendReceiver(internal.AddLoadedModules(version.Module))
	}
	_ = version
	go func() {
		for {
			resp, err := moduleHost.con.GetResponse()
			conn := moduleHost.getCon(resp.ID)
			if conn == nil {
				return
			}

			isFatal := false
			for _, err := range resp.Log {
				if err.Code == errors.CodeFatal {
					isFatal = true
				}
			}

			if conn.ret != nil && !isFatal {
				err := moduleHost.con.GetData(conn.ret)
				if err != nil {
					conn.send <- response{
						resp: resp,
						err:  err,
					}
					continue
				}
			}
			conn.send <- response{
				resp: resp,
				err:  err,
			}
		}
	}()
	return
}

//Kill kills the module
func (m *ModuleHost) Kill(shouldWait ...bool) {
	var id [10]byte
	rand.Read(id[:]) //even if the ID sucks and isnt complete it doesnt matter since we dont expect a response

	m.con.Send(protocol.KillCommand, protocol.Kill{}, id)
	if len(shouldWait) != 0 && shouldWait[1] {
		m.awaitResponse(id)
	}
}

// AskMethods asks for the methods a moduleHost can handle, it returns a methods type
func (m *ModuleHost) AskMethods() (protocol.Methods, error) {

	var id [10]byte
	_, err := rand.Read(id[:])
	if err != nil {
		return nil, ErrFailedToGenID
	}

	ret := protocol.Methods{}

	m.addConnection(id, &ret)
	m.con.Send(protocol.GetMethods, protocol.ReceiveMethods{}, id)

	resp := m.awaitResponse(id)
	m.remCon(id)

	if resp.err != nil {
		return nil, resp.err
	}

	for _, err := range resp.resp.Log {
		switch err.Code {
		case errors.CodeDebug:
			m.Logger.Debug(err.Error())
		case errors.CodeInfo:
			m.Logger.Info(err.Error())
		case errors.CodeWarning:
			m.Logger.Warning(err.Error())
		case errors.CodeFatal:
			m.Logger.Fatal(err.Error())
			return nil, err
		}
	}

	return ret, nil
}

// ExcecuteMethod executes a method and puts the return data in `ret`
func (m *ModuleHost) ExcecuteMethod(function string, args interface{}, ret interface{}, binary ...byte) error {
	if ret != nil && reflect.ValueOf(ret).Kind() != reflect.Ptr {
		return errors.New("return argument must be pointer or nil", errors.CodeFatal)
	}
	var id [10]byte
	_, err := rand.Read(id[:])
	if err != nil {
		return ErrFailedToGenID
	}

	var payload protocol.ExecuteMethod
	payload.Function = function
	payload.Args = args
	m.addConnection(id, ret)
	m.con.Send(protocol.ComExecute, payload, id, binary...)

	resp := m.awaitResponse(id)

	m.remCon(id)
	if resp.err != nil {
		return resp.err
	}
	for _, err := range resp.resp.Log {
		switch err.Code {
		case errors.CodeDebug:
			m.Logger.Debug(err.Error())
		case errors.CodeInfo:
			m.Logger.Info(err.Error())
		case errors.CodeWarning:
			m.Logger.Warning(err.Error())
		case errors.CodeFatal:
			m.Logger.Fatal(err.Error())
			return err
		}
	}

	return nil
}

// ExcecuteMethodAsync executes a method and calls the given callback with the returned data
func (m *ModuleHost) ExcecuteMethodAsync(function string, args interface{}, ret interface{}, callback func(interface{}, error) bool, binary ...byte) error {
	if ret != nil && reflect.ValueOf(ret).Kind() != reflect.Ptr {
		return errors.New("return argument must be pointer or nil", errors.CodeFatal)
	}
	var id [10]byte
	_, err := rand.Read(id[:])
	if err != nil {
		return ErrFailedToGenID
	}

	var payload protocol.ExecuteMethod
	payload.Function = function
	payload.Args = args
	m.addBufferedConnection(id, ret, 100)
	m.con.Send(protocol.ComExecute, payload, id, binary...)

	go func() {
		for {
			var callerr error
			resp := m.awaitResponse(id)
			if resp.err != nil {
				callerr = resp.err
			}
			for _, err := range resp.resp.Log {
				switch err.Code {
				case errors.CodeDebug:
					m.Logger.Debug(err.Error())
				case errors.CodeInfo:
					m.Logger.Info(err.Error())
				case errors.CodeWarning:
					m.Logger.Warning(err.Error())
				case errors.CodeFatal:
					m.Logger.Fatal(err.Error())
					callerr = err
				}
			}
			if !callback(ret, callerr) {
				break
			}
		}
		m.remCon(id)
	}()

	return nil
}

func (m *ModuleHost) addConnection(id protocol.ID, e interface{}) {
	connection := command{}
	connection.send = make(chan response)
	connection.ret = e
	m.setCon(id, &connection)
}
func (m *ModuleHost) addBufferedConnection(id protocol.ID, e interface{}, n int) {
	connection := command{}
	connection.send = make(chan response, n)
	connection.ret = e
	m.setCon(id, &connection)
}

func (m *ModuleHost) awaitResponse(id protocol.ID) response {
	com := m.getCon(id)
	resp := <-com.send
	return resp
}

func (m *ModuleHost) getCon(id protocol.ID) *command {
	m.lock.RLock()
	defer m.lock.RUnlock()
	return m.commands[id]
}

func (m *ModuleHost) setCon(id protocol.ID, com *command) {
	m.lock.Lock()
	defer m.lock.Unlock()
	m.commands[id] = com
}

func (m *ModuleHost) remCon(id protocol.ID) {
	m.lock.Lock()
	defer m.lock.Unlock()
	delete(m.commands, id)
}

func (l logger) Fatal(s string) {
}
func (l logger) Fatalf(s string, v ...interface{}) {
}

func (l logger) Warning(s string) {
}
func (l logger) Warningf(s string, v ...interface{}) {
}

func (l logger) Info(s string) {
}
func (l logger) Infof(s string, v ...interface{}) {
}

func (l logger) Debug(s string) {
}
func (l logger) Debugf(s string, v ...interface{}) {
}
