package site

import (
	"fmt"
	"reflect"
	"testing"

	"github.com/go-test/deep"
	"github.com/jaicewizard/tt"
)

func TestFullEncode(t *testing.T) {
	s := []Site{
		{
			Slug:     "hey",
			Template: "you",
			Data: tt.Data{
				"flower": "pot",
			},
		},
		{
			Slug:     "my",
			Template: "candy",
			Data: tt.Data{
				"is": "sweat",
			},
		},
	}
	bytes := Encode(s)
	fmt.Println(bytes)
	s1 := Decode(bytes)
	if !reflect.DeepEqual(s, s1) {
		fmt.Println(s1)
		fmt.Println(s)
		t.FailNow()
	}
}

func TestFullEncodeMisteryFail(t *testing.T) {
	s := []Site{
		{
			Slug:     "/index.html",
			Template: "PvjrEayfidqdJTVDpNEnDzbqirXnNnVq",
			Data: map[interface{}]interface{}{
				"layout": map[interface{}]interface{}{
					"name": "Antibuild Demo Page",
					"pages": []interface{}{
						map[interface{}]interface{}{
							"title": "Home",
							"slug":  "/",
						},
						map[interface{}]interface{}{
							"title": "News",
							"slug":  "/news",
						},
					},
				},
				"page": map[interface{}]interface{}{
					"title": "Home",
				},
				"test": map[interface{}]interface{}{
					"id": uint64(1),
				},
			},
		},
		{
			Slug:     "/nl/index.html",
			Template: "PvjrEayfidqdJTVDpNEnDzbqirXnNnVq",
			Data: map[interface{}]interface{}{
				"layout": map[interface{}]interface{}{
					"name": "Antibuild Demo Page",
					"pages": []interface{}{
						map[interface{}]interface{}{
							"title": "Home",
							"slug":  "/",
						},
						map[interface{}]interface{}{
							"title": "News",
							"slug":  "/news",
						},
					},
				},
				"page": map[interface{}]interface{}{
					"title": "Home",
				},
				"test": map[interface{}]interface{}{
					"id": uint64(1),
				},
			},
		},
	}

	byts := Encode(s)
	s1 := Decode(byts)
	if !reflect.DeepEqual(s, s1) {
		diff := deep.Equal(s1, s)
		fmt.Println(diff)
		t.FailNow()
	}
}
