package site

import (
	"bytes"

	"github.com/jaicewizard/tt"
)

//Site is the way a site is defined after all of its data and templates have been collected
type Site struct {
	Slug     string
	Template string
	Data     map[interface{}]interface{}
}

func Encode(data []Site) []byte {
	buf := &bytes.Buffer{}
	tt.Encodev3(data, buf)
	return buf.Bytes()
}

func Decode(data []byte) []Site {
	ret := make([]Site, 0)
	tt.Decodev3(bytes.NewBuffer(data), &ret)
	return ret
}
