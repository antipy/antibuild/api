# AntiBuild module API

The API used for creating, and interfacing, with modules written for antibuild

## How to use

All antibuild modules have to import the client
(gitlab.com/antipy/antibuild/api/client), in the examples we import this as
abm (antibuild module).

```go
import (
  abm "gitlab.com/antipy/antibuild/api/client"
)
```

The module has to register a itself with a name:

> This name should only contain upper and lowercase letters, numbers and underscores

```go
module := abm.Register("module_name")
```

This returns a \*Module. using this module you can register functions. For
example for a data parser (json,yaml, etc) this would be:

> This name should only contain upper and lowercase letters, numbers and underscores

```go
module.DataParserRegister("function_name", DataParser)
```

For some function types it is required to provide a test for a funtion. A
template function is one example of this. An example from the math module:

```go
module.TemplateFunctionRegister("add", add, &abm.TFTest{
  Request: abm.TFRequest{
    Data: []interface{}{
      1,
      2,
    }
  },
  Response: &abm.TFResponse{
    Data: 3,
  },
})
```

At the end of your main function you should call module.Start() in order to
start listening for incomming commands.

The functions should receive a Request type that matches the function type, and a Response type. The request will have the nessecary information to process the request. If receive some kind of interface you should always check the type, there are no type guarantees about the underlying types of interface{}. You can respond with data using r.AddData(data), this will check if its the correct type, and return false if it is not the correct type. You should only call r.AddData() once per execution.

You can add errors to return log, any error will result in a failure and results could be disregarded. Only the first error will be returned to the caller. There are 2 types of errors, r.AddWarning() which is meant for non fatal errors and r.AddFatal(message) for when the function can not complete because of a fatal error. You have to manually return out of the function after calling r.AddFatal(). You can also use r.AddInfo() to add some information message to the log, and r.AddDebug() to help debugging. This should be avoided in production, but could, for example, be enabled in a the module config- These might be printed out on the host, depending on the antibuild config.

An example of a complete module would be: (subset of the math module)

```golang
package main

import (
	abm "gitlab.com/antipy/antibuild/api/client"
)

func main() {
	module := abm.Register("math")

	module.TemplateFunctionRegister("add", add, &abm.TFTest{
		Request: abm.TFRequest{Data: []interface{}{
			1,
			2,
		}}, Response: &abm.TFResponse{
			Data: 3,
		},
	})

	module.Start()
}

func add(w abm.TFRequest, r abm.Response) {
	var args = make([]int, len(w.Data))
	var ok bool

	for i, data := range w.Data {
		if args[i], ok = data.(int); !ok {
			r.AddWarning(abm.InvalidInput)
			return
		}
	}

	result := args[0] + args[1]

	r.AddData(result)
	return
}
```

You can look at some more examples in our modules standard library. The repository for these modules is located at gitlab.com/antipy/antibuild/std
